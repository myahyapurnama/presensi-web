<?php
    Class database
    {
        public $host  = "localhost",
               $uname = "root",
               $pass  = "",
               $db    = "presensi-sister",
               $connect;
        function __construct() {
            $this->connect = new mysqli($this->host, $this->uname, $this->pass, $this->db);
            if ($this->connect->connect_errno) {
                echo "Database Tidak Ada ... !";
                exit;
            }
        }

        // CRUD Tabel Pegawai
        function tampil_data_pegawai() {
            $data  = "SELECT *,
                        (SELECT PRESTGL FROM presensi WHERE presensi.PEGNIK=pegawai.PEGNIK ORDER BY PRESTGL DESC LIMIT 1) AS TGL, 
                        (SELECT PRESJAM FROM presensi WHERE presensi.PEGNIK=pegawai.PEGNIK ORDER BY PRESTGL DESC LIMIT 1) AS JAM, 
                        (SELECT DAYNAME(PRESTGL) FROM presensi WHERE presensi.PEGNIK=pegawai.PEGNIK ORDER BY PEGNIK LIMIT 1) AS HARI 
                        FROM pegawai ORDER BY PEGNIK";
            $hasil = $this->connect->query($data);
            while ($id = mysqli_fetch_array($hasil)) {
                $result[] = $id;
            } 
            return $result;
        }
        // function status_absen() {
        //     $data  = "SELECT * FROM pegawai, presensi WHERE pegawai.PEGNIK=presensi.PEGNIK ORDER BY pegawai.PEGNIK";
        //     $hasil = $this->connect->query($data);
        //     while ($id = mysqli_fetch_array($hasil)) {
        //         $result[] = $id;
        //     } 
        //     return $result;
        // }
        function input_pegawai($PEGNIK, $PEGNAMA, $PEGKOTA) {
            $simpan = "INSERT INTO pegawai VALUES('$PEGNIK', '$PEGNAMA', '$PEGKOTA')";
            $hasil  = $this->connect->query($simpan);
        }
        function cek_data_pegawai($PEGNIK) {
            $data  = "SELECT * FROM pegawai WHERE PEGNIK='".$PEGNIK."'";
            $hasil = $this->connect->query($data);
            while ($id = mysqli_fetch_array($hasil)) {
                $result[] = $id;
            } 
            return $result;
        }

        // CRUD Tabel Pegawai
        function input_presensi($PEGNIK, $PRESTGL, $PRESJAM) {
            $simpan = "INSERT INTO presensi VALUES('','$PEGNIK', '$PRESTGL', '$PRESJAM')";
            $hasil  = $this->connect->query($simpan);
        }

        function hari_convert($hari){
         
            switch($hari){
                case 'Sunday':
                    $hari_convert = "Minggu";
                break;
         
                case 'Monday':			
                    $hari_convert = "Senin";
                break;
         
                case 'Tuesday':
                    $hari_convert = "Selasa";
                break;
         
                case 'Wednesday':
                    $hari_convert = "Rabu";
                break;
         
                case 'Thursday':
                    $hari_convert = "Kamis";
                break;
         
                case 'Friday':
                    $hari_convert = "Jumat";
                break;
         
                case 'Saturday':
                    $hari_convert = "Sabtu";
                break;
                
                default:
                    $hari_convert = "Tidak di ketahui";		
                break;
            }
         
            return $hari_convert;
         
        }

        function detail_presensi($PEGNIK) {
            $data  = "SELECT *, DAYNAME(PRESTGL) AS HARI FROM presensi WHERE PEGNIK = '$PEGNIK' ORDER BY PRESTGL";
            $hasil = $this->connect->query($data);
            while ($id = mysqli_fetch_array($hasil)) {
                $result[] = $id;
            } 
            return $result;
        }
    }
?>