<div class="app-header header-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <!-- <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button> -->
            <img width="42" class="rounded-circle" src="../assets/images/avatars/1.jpg" alt="">
        </span>
    </div>
    <!-- <div class="app-header__content">
        <div class="app-header-right">
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <img width="42" class="rounded-circle" src="assets/images/avatars/1.jpg" alt="">
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <ul class="nav flex-column">
                                        <li class="nav-item-header nav-item">Menu Akun</li>
                                        <li class="nav-item"><a href="" class="nav-link"><i class="fa fa-user mr-2"></i> Profil</a></li>
                                        <li class="nav-item"><a href="" class="nav-link"><i class="fa fa-cog mr-2"></i> Pengaturan</a></li>
                                        <li class="nav-item-divider nav-item"></li>
                                        <li class="nav-item-btn nav-item">
                                            <button class="btn-wide btn-shadow btn btn-danger btn-sm"><i class="fa fa-sign-out-alt mr-2"></i> Logout</button>
                                        </li>
                                    </ul>
                                </div>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <h6 tabindex="-1" class="dropdown-header">Menu Akun</h6>
                                    <button type="button" tabindex="0" class="dropdown-item"><i class="fa fa-user mr-2"></i> Profil</button>
                                    <button type="button" tabindex="0" class="dropdown-item"><i class="fa fa-cog mr-2"></i> Pengaturan</button>
                                    <button type="button" tabindex="0" class="dropdown-item"><i class="fa fa-sign-out-alt mr-2"></i> Logout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    </div> -->
</div>