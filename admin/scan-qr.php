<?php
include "../vendor/phpqrcode/qrlib.php";
include '../koneksi.php';
$db = new database();
session_start();
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Scan QR</title>
    <?php $page = "Scan QR"; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="../css/admin-main.css" rel="stylesheet">
    <link href="../css/admin-main2.css" rel="stylesheet">
    <link rel="shortcut icon" href="../assets/images/presensi.png">
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'header.php'; ?>
        <div class="app-main">
            <?php include 'sidebar.php'; ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-qrcode icon-gradient bg-malibu-beach">
                                    </i>
                                </div>
                                <div>Scan QR
                                    <div class="page-title-subheading">Halaman untuk mengkonfirmasi presensi Pegawai.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home mr-1"></i> Beranda</a></li>
                            <li class="breadcrumb-item"><a href="data-pegawai.php">Data Pegawai</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Scan QR</li>
                        </ol>
                    </nav>
                    <div class="mb-3 card">
                        <div class="tab-content p-5">
                            <div class="text-center">
                                <h2><i class="fa fa-qrcode"></i> PRESENSI SCAN QR CODE</h2>
                                <br>
                                <hr>
                                <div id="mulaiscan">
                                    <button class="btn btn-primary" id="startqr"><i class="fa fa-qrcode"></i> Scan QR</button>
                                    <button class="btn btn-primary" id="uploadqr"><i class="fa fa-upload"></i> Upload QR</button>
                                </div>
                                <br>
                                <div id="videoscan" class="hilang">
                                    <video id="video" width="300" height="200" style="border: 1px solid gray"></video>
                                    <br><br>
                                    <div>
                                        <button class="btn btn-primary" id="startButton"><i class="fa fa-play"></i> Mulai Scan</button>
                                        <button class="btn btn-danger" id="stopButton"><i class="fa fa-stop"></i> Stop Scan</button>
                                    </div>
                                    <br>
                                    <div id="sourceSelectPanel" style="display:none">
                                        <label for="sourceSelect">Pilih kamera:</label>
                                        <select id="sourceSelect" style="max-width:400px">
                                        </select>
                                    </div>
                                </div>
                                <div id="uploadscan" class="hilang">
                                    <img id="myimage" style="display:none;" height="200" style="border: 1px solid gray;"><br>
                                    <input type="file" onchange="onFileSelected(event)" id="uploadimg"><br>
                                    <br>
                                    <div>
                                        <button class="btn btn-primary" id="decodeButton"><i class="fa fa-sync"></i> Konversi</button>
                                        <button class="btn btn-danger" id="stopButton2"><i class="fa fa-times"></i> Tutup</button>
                                    </div>
                                    <br>
                                </div>
                                <span class="badge badge-primary">
                                    Hasil : <span id="result"><i class="fa fa-question-circle"></i></span>
                                </span>
                                <br>
                                <br>
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                    <input type="hidden" name="scannik" id="scannik">
                                    <button class="btn btn-primary"><i class="fa fa-search"></i> Cek Data</button>
                                </form>
                                <br>
                                <?php
                                    if ($_SERVER["REQUEST_METHOD"] == "POST"){
                                        $conn = mysqli_connect("localhost","root","","presensi-sister");
                                        $PEGNIK = $_POST['scannik'];
                                        $data  = "SELECT * FROM pegawai WHERE PEGNIK='$PEGNIK'";
                                        $hasil = $db->connect->query($data);
                                        $count  = mysqli_num_rows($hasil);
                                        if($count>0) {?>
                                                <div class="table-responsive">
                                                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                                        <thead>
                                                        <tr>
                                                            <?php
                                                                date_default_timezone_set('Asia/Jakarta');
                                                                $date = date('Y-m-d', time());
                                                                $time = date('H:i:s', time());
                                                            ?>
                                                            <th class="text-center" style="width:10%;"><i class="fa fa-id-card mr-1"></i> NIK</th>
                                                            <th class="text-center" style="width:20%;"><i class="fa fa-user mr-1"></i> Nama</th>
                                                            <th class="text-center" style="width:15%;"><i class="fa fa-city mr-1"></i> Asal Kota</th>
                                                            <th class="text-center" style="width:20%;"><i class="fa fa-calendar mr-1"></i> Tanggal</th>
                                                            <th class="text-center" style="width:20%;"><i class="fa fa-clock mr-1"></i> Jam</th>
                                                            <th class="text-center" style="width:15%;"><i class="fa fa-cog mr-1"></i> Aksi</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                foreach($db->cek_data_pegawai($PEGNIK) as $data) {
                                                            ?>
                                                            <tr>
                                                                <td class="text-center"><?php echo $data['PEGNIK'] ?></td>
                                                                <td class="text-center"><?php echo $data['PEGNAMA'] ?></td>
                                                                <td class="text-center"><?php echo $data['PEGKOTA'] ?></td>
                                                                <td class="text-center"><?php echo $date ?></td>
                                                                <td class="text-center"><?php echo $time ?></td>
                                                                <td class="text-center">
                                                                    <a type="button" href="../proses.php?aksi=tambah-presensi&PEGNIK=<?php echo $data['PEGNIK'] ?>&PRESTGL=<?php echo $date ?>&PRESJAM=<?php echo $time ?>" tabindex="0" class="btn btn-success"><i class="fa fa-check-circle mr-2"></i> Presensi</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            echo "Data tidak ditemukan";
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../assets/scripts/main.js"></script>
    <script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
    <script type="text/javascript">
        window.addEventListener('load', function() {
            let selectedDeviceId;
            const codeReader = new ZXing.BrowserMultiFormatReader()
            console.log('ZXing code reader initialized')
            codeReader.listVideoInputDevices().then((videoInputDevices) => {
                const sourceSelect = document.getElementById('sourceSelect')
                selectedDeviceId = videoInputDevices[0].deviceId
                if (videoInputDevices.length >= 0) {
                    videoInputDevices.forEach((element) => {
                        const sourceOption = document.createElement('option')
                        sourceOption.text = element.label
                        sourceOption.value = element.deviceId
                        sourceSelect.appendChild(sourceOption)
                    })

                    sourceSelect.onchange = () => {
                        selectedDeviceId = sourceSelect.value
                    }

                    const sourceSelectPanel = document.getElementById('sourceSelectPanel')
                    sourceSelectPanel.style.display = 'block'
                }

                    document.getElementById('startButton').addEventListener('click', () => {
                        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                            if (result) {
                                console.log(result)
                                document.getElementById('result').textContent = result.text
                                document.getElementById('scannik').value = result.text
                            }
                            if (err && !(err instanceof ZXing.NotFoundException)) {
                                console.error(err)
                                document.getElementById('result').textContent = err
                            }
                        })
                        console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
                    })
                    document.getElementById('stopButton').addEventListener('click', () => {
                        codeReader.reset()
                        console.log('Reset.')
                        document.getElementById('mulaiscan').classList.remove('hilang')
                        document.getElementById('videoscan').classList.add('hilang')
                    })
                })
                .catch((err) => {
                    console.error(err)
                })
        })
    </script>

    <script>
        window.addEventListener('load', function () {
            const codeReader = new ZXing.BrowserQRCodeReader()
            console.log('ZXing code reader initialized')

            document.getElementById('decodeButton').addEventListener('click', () => {
                const img = document.getElementById('myimage')
                codeReader.decodeFromImage(img).then((result) => {
                    console.log(result)
                    document.getElementById('result').textContent = result.text
                    document.getElementById('scannik').value = result.text
                }).catch((err) => {
                    console.error(err)
                    document.getElementById('result').textContent = err
                })
                console.log(`Started decode for image from ${img.src}`)
            })

        })
        document.getElementById('stopButton2').addEventListener('click', () => {
            document.getElementById('uploadimg').value = '';
            document.getElementById('myimage').style.display = "none";
            document.getElementById('mulaiscan').classList.remove('hilang')
            document.getElementById('uploadscan').classList.add('hilang')
        })
    </script>

    <script>
        document.getElementById('uploadqr').addEventListener('click', () => {
            document.getElementById('mulaiscan').classList.add('hilang')
            document.getElementById('uploadscan').classList.remove('hilang')
        })
        document.getElementById('startqr').addEventListener('click', () => {
            document.getElementById('mulaiscan').classList.add('hilang')
            document.getElementById('videoscan').classList.remove('hilang')
        })
        function onFileSelected(event) {
            document.getElementById('myimage').style.removeProperty('display');
            var selectedFile = event.target.files[0];
            var reader = new FileReader();

            var imgtag = document.getElementById("myimage");
            imgtag.title = selectedFile.name;

            reader.onload = function(event) {
                imgtag.src = event.target.result;
            };

            reader.readAsDataURL(selectedFile);
        }
    </script>
</body>

</html>