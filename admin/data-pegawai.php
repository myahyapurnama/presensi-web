<?php
    include '../koneksi.php';
    $db = new database();
    session_start();
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Data Pegawai</title>
    <?php $page = "Data Pegawai"; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="../css/admin-main.css" rel="stylesheet">
    <link rel="shortcut icon" href="../assets/images/presensi.png">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'header.php'; ?>
        <div class="app-main">
            <?php include 'sidebar.php'; ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-user icon-gradient bg-malibu-beach">
                                    </i>
                                </div>
                                <div>Data Pegawai
                                    <div class="page-title-subheading">Berisi halaman yang memuat seluruh data Pegawai.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home mr-1"></i> Beranda</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Data Pegawai</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-header"><i class="fa fa-list mr-2"></i> List Pegawai
                                    <div class="btn-actions-pane-right">
                                        <a href="scan-qr.php" class="btn btn-success">
                                            <i class="fa fa-qrcode mr-2"></i> Scan QR</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center" style="width:10%;"><i class="fa fa-asterisk mr-1"></i> No</th>
                                            <th style="width:25%;"><i class="fa fa-user mr-1"></i> Nama</th>
                                            <th class="text-center" style="width:20%;"><i class="fa fa-info-circle mr-1"></i> Status Absen Hari Ini</th>
                                            <th class="text-center" style="width:30%;"><i class="fa fa-calendar mr-1"></i> Terakhir Absen</th>
                                            <th class="text-center" style="width:15%;"><i class="fa fa-cog mr-1"></i> Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1; foreach($db->tampil_data_pegawai() as $data) { ?>
                                                <tr>
                                                    <td class="text-center text-muted"><?php echo $no; $no++; ?></td>
                                                    <td>
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading"><?php echo $data['PEGNAMA']; ?></div>
                                                                    <div class="widget-subheading opacity-7"><?php echo $data['PEGNIK']; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <!-- <td class="text-center"><?php echo date('j F Y', strtotime($data['TRTGL'])); ?></td> -->
                                                    <!-- <td class="text-center"><span class="badge <?php echo $badge; ?>" style="width:75px;"><?php echo $data['TRSTATUS'] ?></span></td> -->
                                                    
                                                    <?php
                                                        // $date_now = date("Y-m-d");
                                                        // if($data['TGL'] >= $date_now){
                                                            if($data['JAM'] >= '08:00:00'){
                                                                $warnabadge = "badge-danger";
                                                                $isibadge = "Terlambat";
                                                            } elseif($data['JAM'] <= '08:00:00'){
                                                                $warnabadge = "badge-success";
                                                                $isibadge = "Tepat Waktu";
                                                            } else {
                                                                $warnabadge = "badge-warning";
                                                                $isibadge = "Belum Absen";
                                                            }
                                                        // } else {
                                                        //     $warnabadge = "badge-dark";
                                                        //     $isibadge = "Bolos";
                                                        // }
                                                    ?>
                                                    
                                                    <td class="text-center"><span class="badge <?php echo $warnabadge; ?>" style="width:100px;"><?php echo $isibadge; ?></span></td>
                                                    <td class="text-center">
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading"><?php echo $db->hari_convert($data['HARI']) . ', ' . date("d-m-Y", strtotime($data['TGL'])) ?></div>
                                                                    <div class="widget-subheading opacity-7"><?php echo $data['JAM'] ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <!-- <td class="text-center"><?php echo $db->hari_convert($data['HARI']) . ', ' . date("d-m-Y", strtotime($data['TGL'])) . ' ' . $data['JAM']; ?></td> -->
                                                    <td class="text-center">
                                                        <div class="dropdown d-inline-block">
                                                            <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle btn btn-primary"><i class="fa fa-th mr-1"></i> Aksi</button>
                                                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu">
                                                                <button type="button" onclick="window.location.href='detail-presensi.php?PEGNIK=<?php echo $data['PEGNIK']; ?>'" tabindex="0" class="dropdown-item text-info"><i class="fa fa-search mr-3"></i> Presensi</button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
<script type="text/javascript" src="../assets/scripts/main.js"></script></body>
</html>
