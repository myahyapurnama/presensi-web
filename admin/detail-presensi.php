<?php
    include "../vendor/phpqrcode/qrlib.php"; 
    include '../koneksi.php';
    $db = new database();
    session_start();
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Detail Presensi</title>
    <?php $page = "Detail Presensi"; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="../css/admin-main.css" rel="stylesheet">
    <link rel="shortcut icon" href="../assets/images/presensi.png">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'header.php'; ?>
        <div class="app-main">
            <?php include 'sidebar.php'; ?> 
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="fa fa-search icon-gradient bg-malibu-beach">
                                    </i>
                                </div>
                                <div>Detail Presensi
                                    <div class="page-title-subheading">Halaman yang memuat Detail Presensi berdasarkan NIK Pegawai.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php"><i class="fa fa-home mr-1"></i> Beranda</a></li>
                            <li class="breadcrumb-item"><a href="data-transaksi.php">Data Pegawai</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Detail Presensi</li>
                        </ol>
                    </nav>
                    <div class="mb-3 card">
                        <div class="tab-content p-5">
                            <div class="h2">
                                <span class="h2">PRESENSI <span class="badge badge-primary"><?php echo $_GET['PEGNIK'] ?></span></span>
                                
                                <span class="btn-actions-pane-right float-right">
                                    <a href="data-pegawai.php" class="btn btn-danger">
                                        <i class="fa fa-arrow-left mr-2"></i> Kembali</a>
                                </span>
                            </div>
                            <?php 
                                $tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
                                if (!file_exists($tempdir)){
                                    //Buat folder bername temp
                                    mkdir($tempdir);
                                }

                                foreach($db->cek_data_pegawai($_GET['PEGNIK']) as $data) { 
                                    
                                $codeContents = $data['PEGNIK']; 
                                    
                                QRcode::png($codeContents, $tempdir.'006_H.png', QR_ECLEVEL_H, 10);
                                
                                                        
                                echo '<img src="'.$tempdir.'006_H.png" style="width:30%; height:30%" />'; 
                            ?>
                            <br>
                            <label class="text-primary font-weight-bold"><i class="fas fa-fw fa-user"></i> DATA PEGAWAI</label>
                            <table class="table table-striped lead" id="dataTable" width="100%" cellspacing="0">
                                <tr>
                                    <td style="width: 30%"><i class="fas fa-fw fa-id-card ml-2 mr-1"></i> NIK</td>
                                    <td style="width: 80%"><?php echo $data['PEGNIK'] ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%"><i class="fas fa-fw fa-user ml-2 mr-1"></i> Nama</td>
                                    <td style="width: 80%"><?php echo $data['PEGNAMA'] ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%"><i class="fas fa-fw fa-city ml-2 mr-1"></i> Kota Tinggal</td>
                                    <td style="width: 80%"><?php echo $data['PEGKOTA'] ?></td>
                                </tr>
                            </table>
                            <?php } ?>
                            <label class="text-primary font-weight-bold"><i class="fas fa-fw fa-briefcase"></i> DATA PRESENSI</label>
                            <div class="table-responsive">
                                <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width:10%;"><i class="fa fa-asterisk mr-1"></i> No</th>
                                        <th class="text-center" style="width:20%;"><i class="fa fa-calendar-day mr-1"></i> Hari</th>
                                        <th class="text-center" style="width:30%;"><i class="fa fa-calendar mr-1"></i> Tanggal</th>
                                        <th class="text-center" style="width:20%;"><i class="fa fa-clock mr-1"></i> Jam</th>
                                        <th class="text-center" style="width:20%;"><i class="fa fa-info-circle mr-1"></i> Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1; foreach($db->detail_presensi($_GET['PEGNIK']) as $data) { ?>
                                            <tr>
                                                <?php
                                                    // $date_now = date("Y-m-d");
                                                    // if($data['TGL'] >= $date_now){
                                                        if($data['PRESJAM'] >= '08:00:00'){
                                                            $warnabadge = "badge-danger";
                                                            $isibadge = "Terlambat";
                                                        } elseif($data['PRESJAM'] <= '08:00:00'){
                                                            $warnabadge = "badge-success";
                                                            $isibadge = "Tepat Waktu";
                                                        } else {
                                                            $warnabadge = "badge-warning";
                                                            $isibadge = "Belum Absen";
                                                        }
                                                    // } else {
                                                    //     $warnabadge = "badge-dark";
                                                    //     $isibadge = "Bolos";
                                                    // }
                                                ?>
                                                <td class="text-center text-muted"><?php echo $no; $no++; ?></td>
                                                <td class="text-center"><?php echo $db->hari_convert($data['HARI']) ?></td>
                                                <td class="text-center"><?php echo date("d-m-Y", strtotime($data['PRESTGL'])) ?></td>
                                                <td class="text-center"><?php echo $data['PRESJAM'] ?></td>
                                                <td class="text-center"><span class="badge <?php echo $warnabadge; ?>" style="width:100px;"><?php echo $isibadge; ?></span></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
<script type="text/javascript" src="../assets/scripts/main.js"></script></body>
</html>
