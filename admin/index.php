<?php
    include 'koneksi.php';
    $db = new database();
    session_start();
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Beranda</title>
    <?php $page = "Beranda"; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="./main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/images/Laundry.png">
</head>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <?php include 'header.php'; ?>
        <div class="app-main">
            <?php include 'sidebar.php'; ?>
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-home icon-gradient bg-malibu-beach">
                                    </i>
                                </div>
                                <div>Beranda
                                    <div class="page-title-subheading">Dashboard Aplikasi Laundry Berbasis Web.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="active breadcrumb-item" aria-current="page"><i class="fa fa-home mr-1"></i> Beranda</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-arielle-smile">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading"><i class="fa fa-shopping-cart mr-1"></i> Total Transaksi</div>
                                        <div class="widget-subheading">Transaksi Dilakukan</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white"><span><?php echo $db->tampil_total_trx(); ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-grow-early">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading"><i class="fa fa-money-bill mr-1"></i> Total Pendapatan</div>
                                        <div class="widget-subheading">Pendapatan Diperoleh</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="text-white lead"><span><?php echo 'Rp ' . number_format($db->tampil_total_uang(),0,',','.'); ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-4">
                            <div class="card mb-3 widget-content bg-midnight-bloom">
                                <div class="widget-content-wrapper text-white">
                                    <div class="widget-content-left">
                                        <div class="widget-heading"><i class="fa fa-users mr-1"></i> Total Customer</div>
                                        <div class="widget-subheading">Customer Berlangganan</div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-white"><span><?php echo $db->tampil_total_customer(); ?></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 card">
                        <div class="text-center tab-content">
                            <div class="h1 pt-5"><img src="assets/images/Laundry.png" alt="icon-laundry-index" style="height:20%; width:20%;"></div>
                            <div class="h1 pt-4">APLIKASI LAUNDRY BERBASIS WEB</div>
                            <hr class="sidebar-divider mb-5" style="width:50%;">
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
<script type="text/javascript" src="./assets/scripts/main.js"></script></body>
</html>
