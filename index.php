<?php
    include "vendor/phpqrcode/qrlib.php"; 
    include 'koneksi.php';
    $db = new database();
	session_start();
	
	$_SESSION['alertnik'] = "";
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Login Pegawai</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
    <link rel="shortcut icon" href="assets/images/presensi.png">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/login-util.css">
	<link rel="stylesheet" type="text/css" href="css/login-main.css">
	<!--===============================================================================================-->


	<script type="text/javascript">
		function NIKNotFound(){
			document.getElementById("peringatan").innerHTML = "<div class='alert alert-danger text-center' role='alert'><b><i class='fa fa-info-circle'></i> NIK tidak ditemukan</b></div>";
		}
	</script>
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="login100-form validate-form" method="POST">
					<h1 class="h1 text-center"><b>PRESENSI PEGAWAI</b></h1>
					<span class="login100-form-title p-b-43">
						<b><i class="fa fa-qrcode"></i> QR CODE</b>
					</span>

					<div id="peringatan"><div class='alert alert-primary text-center' role='alert'><b><i class='fa fa-info-circle'></i> Silahkan masukkan NIK</b></div></div>

					<div class="wrap-input100 validate-input" data-validate="Kolom NIK masih kosong">
						<input class="input100" type="text" name="PEGNIK">
						<span class="focus-input100"></span>
						<span class="label-input100">NIK</span>
					</div>


					<div class="container-login100-form-btn">
						<input class="login100-form-btn" type="submit" value="Generate">
					</div>

					<br>
					<div id="qrcode"></div>

					<?php
						if ($_SERVER["REQUEST_METHOD"] == "POST"){
							$conn = mysqli_connect("localhost","root","","presensi-sister");
							$PEGNIK = $_POST['PEGNIK'];
							$result = mysqli_query($conn,"SELECT * FROM pegawai WHERE PEGNIK='" . $PEGNIK . "'");
							$row = mysqli_fetch_array($result);
							$count  = mysqli_num_rows($result);
							if($count>0) {
								$tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
								if (!file_exists($tempdir)){
									//Buat folder bername temp
									mkdir($tempdir);
								}
								$codeContents = $PEGNIK;
								QRcode::png($codeContents, $tempdir.'006_H.png', QR_ECLEVEL_H, 10);
								echo '<div class="text-center" style="display:flex;align-items: center;justify-content: center;">DATA PEGAWAI :<br>'.$row['PEGNIK'].'<br>'.$row['PEGNAMA'].'<br>'.$row['PEGKOTA'].'<br><span class="badge badge-dark"><b>SCAN<br>QR CODE <i class="fa fa-arrow-right"></i></b></span><img src="'.$tempdir.'006_H.png" style="width:40%; height:35%" /></div>';
							} else {
								echo '<script>NIKNotFound();</script>';
							}
						}
					?>

					<div class="text-center p-t-46 p-b-20">
						<span class="txt1">
							Belum punya akun? <a href="register.php">Klik Di sini!</a>
						</span>
					</div>
				</form>

				

				<div class="login100-more" style="background-image: url('assets/images/bg-01.jpg');">
				</div>
			</div>
		</div>
	</div>


	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/login-main.js"></script>

</body>

</html>