<?php
    include 'koneksi.php';
    $db = new database();
    session_start();
    
    $aksi = $_GET['aksi'];

    // CRUD Tabel Pegawai
    if ($aksi == "tambah-pegawai") {
        $id_aspek = $_POST['PEGNIK'];
        $conn = mysqli_connect("localhost","root","","presensi-sister");
        $query = mysqli_query($conn,"SELECT * FROM pegawai WHERE id_aspek='" . $id_aspek . "'");
        $count = mysqli_num_rows($query);
        if($count == 0) {
            $db->input_pegawai($_POST['PEGNIK'], $_POST['PEGNAMA'], $_POST['PEGKOTA']);
            header("location:index.php");
        } else {
            $_SESSION['alertnik'] = "<div class='alert alert-danger text-center' role='alert'>
                <b><i class='fas fa-fw fa-exclamation-triangle'></i> NIK sudah terpakai!</b>
            </div>";
            header("location:register.php");
        }
    }

    // CRUD Tabel Absen
    if ($aksi == "tambah-presensi") {
        $db->input_presensi($_GET['PEGNIK'], $_GET['PRESTGL'], $_GET['PRESJAM']);
        header("location:admin/scan-qr.php");
    }


    // Bagian Logout
    elseif ($aksi == "logout") {
        session_destroy();
        header("location: index.php"); // redirect ke halaman login
        exit();
    }
?>