<?php
    include 'koneksi.php';
    $db = new database();
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>Daftar Pegawai</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
    <link rel="shortcut icon" href="assets/images/presensi.png">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/login-util.css">
	<link rel="stylesheet" type="text/css" href="css/login-main.css">
	<!--===============================================================================================-->
    <link rel="shortcut icon" href="assets/images/presensi.png">
</head>

<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="proses.php?aksi=tambah-pegawai" class="login100-form validate-form" method="POST" enctype="multipart/form-data">
					<h1 class="h1 text-center"><b>PRESENSI PEGAWAI</b></h1>
					<span class="login100-form-title p-b-43">
						<b><i class="fa fa-user-plus"></i> REGISTER</b>
					</span>

                 	<?php echo $_SESSION['alertnik'] ?>
					<div class="wrap-input100 validate-input" data-validate="Kolom NIK masih kosong">
						<input class="input100" type="text" name="PEGNIK">
						<span class="focus-input100"></span>
						<span class="label-input100">NIK</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Kolom Nama masih kosong">
						<input class="input100" type="text" name="PEGNAMA">
						<span class="focus-input100"></span>
						<span class="label-input100">Nama</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Kolom Kota masih kosong">
						<input class="input100" type="text" name="PEGKOTA">
						<span class="focus-input100"></span>
						<span class="label-input100">Kota</span>
					</div>


					<div class="container-login100-form-btn">
						<input class="login100-form-btn" type="submit" value="Daftar">
					</div>

					<div class="text-center p-t-46 p-b-20">
						<span class="txt1">
							Sudah punya akun? <a href="index.php">Klik Di sini!</a>
						</span>
					</div>
				</form>

				<div class="login100-more" style="background-image: url('assets/images/bg-01.jpg');">
				</div>
			</div>
		</div>
	</div>





	<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="js/login-main.js"></script>

</body>

</html>